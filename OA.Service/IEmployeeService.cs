﻿using OA.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace OA.Service
{
    public interface IEmployeeService
    {
        IEnumerable<Employee> GetEmployees(string firstName, string lastName);
        Employee GetEmployee(int id);
        void PutEmployee(int id, Employee employee);
        void PostEmployee(Employee employee);
        Employee DeleteEmployee(int id);
        bool IsEmployeeIdUnique(string employeeId);
        bool EmployeeIdExists(int id);
    }
}
