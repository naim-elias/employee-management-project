﻿using Microsoft.EntityFrameworkCore;
using OA.Data;
using OA.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace OA.Service
{
    public class EmployeeService : IEmployeeService
    {
        private IRepository<Employee> employeeRepository;

        public EmployeeService(IRepository<Employee> employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        public IEnumerable<Employee> GetEmployees(string firstName, string lastName)
        {
            IEnumerable<Employee> employees = employeeRepository.GetAll();

            if (!String.IsNullOrEmpty(firstName))
            {
                employees = employees.Where(t => t.FirstName.Contains(firstName));
            }
            if (!String.IsNullOrEmpty(lastName))
            {
                employees = employees.Where(t => t.LastName.Contains(lastName));
            }
            return employees.ToList();
        }

        public Employee GetEmployee(int id)
        {
            return employeeRepository.Get(id);
        }

        public void PutEmployee(int id, Employee employee)
        {
            if (!EmployeeIdExists(id))
            {
                throw new InvalidOperationException();
            }

            Employee employeeBeforeUpdating = employeeRepository.Get(id);

            if (employeeBeforeUpdating.EmployeeId != employee.EmployeeId
                && !IsEmployeeIdUnique(employee.EmployeeId))
            {
                throw new InvalidOperationException();
            }

            try
            {
                foreach (PropertyInfo property in typeof(Employee).GetProperties().Where(p => p.CanWrite))
                {
                    property.SetValue(employeeBeforeUpdating, property.GetValue(employee, null), null);
                }
                employeeRepository.Update(employeeBeforeUpdating);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void PostEmployee(Employee employee)
        {
            if (IsEmployeeIdUnique(employee.EmployeeId))
            {
                employeeRepository.Insert(employee);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public Employee DeleteEmployee(int id)
        {
            return employeeRepository.Delete(employeeRepository.Get(id));
        }

        public bool IsEmployeeIdUnique(string employeeId)
        {
            IEnumerable<Employee> employees = employeeRepository.GetAll();
            return !(employees.Any(e => e.EmployeeId == employeeId));
        }
        public bool EmployeeIdExists(int id)
        {
            IEnumerable<Employee> employees = employeeRepository.GetAll();
            return employees.Any(e => e.Id == id);
        }
    }
}
