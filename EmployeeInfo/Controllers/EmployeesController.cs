﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeeInfo.Models;
using System.Text.RegularExpressions;
using System.Reflection;

namespace EmployeeInfo.Controllers
{
    [Route("api/Employees")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly EmployeeContext _context;

        public EmployeesController(EmployeeContext context)
        {
            _context = context;
        }

        // Synchronous Coding
        [HttpGet]
        public ActionResult<IEnumerable<Employee>> GetEmployees(string firstName, string lastName)
        {
            IQueryable<Employee> employees = _context.Employees;

            if (!String.IsNullOrEmpty(firstName))
            {
                employees = employees.Where(t => t.FirstName.Contains(firstName));
            }
            if (!String.IsNullOrEmpty(lastName))
            {
                employees = employees.Where(t => t.LastName.Contains(lastName));
            }
            return employees.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Employee> GetEmployee(int id)
        {
            var employee = _context.Employees.Find(id);

            if (employee == null)
            {
                return NotFound();
            }

            return employee;
        }

        [HttpPut("{id}")]
        public IActionResult PutEmployee(int id, Employee employee)
        {
            if (id != employee.Id || !EmployeeIdExists(id))
            {
                return BadRequest();
            }

            Employee employeeBeforeUpdating = _context.Employees.Find(id);

            if (employeeBeforeUpdating.EmployeeId != employee.EmployeeId
                && !IsEmployeeIdUnique(employee.EmployeeId))
            {
                return BadRequest();
            }

            try
            {
                foreach (PropertyInfo property in typeof(Employee).GetProperties().Where(p => p.CanWrite))
                {
                    property.SetValue(employeeBeforeUpdating, property.GetValue(employee, null), null);
                }
                _context.Employees.Update(employeeBeforeUpdating);
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        [HttpPost]
        public IActionResult PostEmployee(Employee employee)
        {
            if (IsEmployeeIdUnique(employee.EmployeeId))
            {
                _context.Employees.Add(employee);
                _context.SaveChanges();
            }
            else
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult<Employee> DeleteEmployee(int id)
        {
            var employee = _context.Employees.Find(id);
            if (employee == null)
            {
                return NotFound();
            }

            _context.Employees.Remove(employee);
            _context.SaveChanges();

            return employee;
        }

        //Asynchronous Coding
        /*[HttpGet]
        public async Task<ActionResult<IEnumerable<Employee>>> GetEmployees(string firstName, string lastName)
        {
            IQueryable<Employee> employees = _context.Employees;

            if (!String.IsNullOrEmpty(firstName))
            {
                employees = employees.Where(t => t.FirstName.Contains(firstName));
            }
            if (!String.IsNullOrEmpty(lastName))
            {
                employees = employees.Where(t => t.LastName.Contains(lastName));
            }
            return await employees.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Employee>> GetEmployee(int id)
        {
            var employee = await _context.Employees.FindAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            return employee;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployee(int id, Employee employee)
        {
            if (id != employee.Id || !EmployeeIdExists(id))
            {
                return BadRequest();
            }

            Employee employeeBeforeUpdating = await _context.Employees.FindAsync(id);

            if (employeeBeforeUpdating.EmployeeId != employee.EmployeeId
                && !IsEmployeeIdUnique(employee.EmployeeId))
            {
                return BadRequest();
            }

            try
            {
                foreach (PropertyInfo property in typeof(Employee).GetProperties().Where(p => p.CanWrite))
                {
                    property.SetValue(employeeBeforeUpdating, property.GetValue(employee, null), null);
                }
                _context.Employees.Update(employeeBeforeUpdating);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> PostEmployee(Employee employee)
        {
            if (IsEmployeeIdUnique(employee.EmployeeId))
            {
                _context.Employees.Add(employee);
                await _context.SaveChangesAsync();
            }
            else
            {
                // "I can't even handle your request because something is fundamentally wrong.
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Employee>> DeleteEmployee(int id)
        {
            var employee = await _context.Employees.FindAsync(id );
            if (employee == null)
            {
                return NotFound();
            }
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();

            return employee;
        }*/

        private bool IsEmployeeIdUnique(string employeeId)
        {
            return !(_context.Employees.Any(e => e.EmployeeId == employeeId));
        }

        private bool EmployeeIdExists(int id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }
    }
}
