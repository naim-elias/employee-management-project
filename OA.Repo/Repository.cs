﻿using Microsoft.EntityFrameworkCore;
using OA.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OA.Repo
{
    public class Repository<T> : IRepository<T> where T : Employee
    {
        private readonly EmployeeContext context;
        private DbSet<T> Employees;

        public Repository(EmployeeContext context)
        {
            this.context = context;
            Employees = context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return Employees.AsEnumerable();
        }

        public T Get(int id)
        {
            T employee = Employees.Find(id);
            if(employee == null)
            {
                throw new ArgumentOutOfRangeException();
            }
            return employee;
        }

        public void Insert(T employee)
        {
            if (employee == null)
            {
                throw new ArgumentNullException();
            }
            Employees.Add(employee);
            context.SaveChanges();
        }

        public void Update(T employee)
        {
            if (employee == null)
            {
                throw new ArgumentNullException();
            }
            Employees.Update(employee);
            context.SaveChanges();
        }

        public T Delete(T employee)
        {
            if (employee == null)
            {
                throw new ArgumentNullException();
            }
            Employees.Remove(employee);
            context.SaveChanges();

            return employee;
        }

    }
}
