﻿using OA.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace OA.Repo
{
    public interface IRepository<T> where T : Employee
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        void Insert(T employee);
        void Update(T employee);
        T Delete(T employee);
    }
}
