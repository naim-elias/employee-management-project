﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement.Models
{
    public class Employee
    {
        public int Id { get; set; }

        [RegularExpression(@"^[a-zA-Z0-9]*$")]
        [Required]
        public string EmployeeId { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Gender { get; set; }

        public string Religion { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime? DateofBirth { get; set; }

        [Required]
        public string EmailOfficial { get; set; }

        public string EmailPersonal { get; set; }

        public string PhonePrimary { get; set; }

        public string PhoneSecondary { get; set; }

        public string Address { get; set; }

        [Required]
        public string EmploymentType { get; set; }

        [Required]
        public string Designation { get; set; }

        [Required]
        public string Department { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime? JoiningDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? ResignationDate { get; set; }
    }
}
