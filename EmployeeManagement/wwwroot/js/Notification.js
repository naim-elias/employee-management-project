﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/notificationHub").build();

//Disable send button until connection is established
/*document.getElementById("submitButton").disabled = true;*/

connection.on("ReceiveMessage", function (message) {
    alert(message);
});

connection.start().then(function () {
    /*document.getElementById("submitButton").disabled = false;*/
}).catch(function (err) {
    return console.error(err.toString());
});

/*document.getElementById("submitButton").addEventListener("click", function (event) {
    connection.invoke("SendMessage").catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});*/