﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OA.Data;
using OA.Service;

namespace OA.Web.Controllers
{
    [Route("api/Employee")]
    [ApiController]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Employee>> GetEmployees(string firstName, string lastName)
        {
            return employeeService.GetEmployees(firstName, lastName).ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Employee> GetEmployee(int id)
        {
            return employeeService.GetEmployee(id);
        }

        [HttpPut("{id}")]
        public IActionResult PutEmployee(int id, Employee employee)
        {
            if (id != employee.Id)
            {
                return BadRequest();
            }

            employeeService.PutEmployee(id, employee);

            return NoContent();
        }

        [HttpPost]
        public IActionResult PostEmployee(Employee employee)
        {
            employeeService.PostEmployee(employee);
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult<Employee> DeleteEmployee(int id)
        {
            return employeeService.DeleteEmployee(id);
        }
    }
}