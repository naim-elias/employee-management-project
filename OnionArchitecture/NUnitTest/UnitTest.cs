using Autofac.Extras.Moq;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using OA.Data;
using OA.Repo;
using OA.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NUnitTest
{
    public class Tests
    {
        private IEmployeeService _employeeService;
        private AutoMock _mock;
        private Mock<IRepository<Employee>> _iRepositoryMock;

        [OneTimeSetUp]
        public void ClassSetup()
        {
            _mock = AutoMock.GetLoose();
        }

        [OneTimeTearDown]
        public void ClassCleanUp()
        {
            _mock?.Dispose();
        }

        [SetUp]
        public void TestSetup()
        {
            _iRepositoryMock = _mock.Mock<IRepository<Employee>>();
            _employeeService = _mock.Create<EmployeeService>();
        }

        [TearDown]
        public void TestCleanUp()
        {
            _iRepositoryMock.Reset();
        }

        [Test, Category("Insert")]
        public void PostEmployee_CreateNewEmployee_AddEmployee()
        {
            //Arrange
            List<Employee> employees = new List<Employee>();

            var employee = new Employee
            {
                Id = 1,
                EmployeeId = "EMP00001",
                FirstName = "Naim",
                LastName = "Elias",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            _iRepositoryMock.Setup(x => x.Insert(It.Is<Employee>(y => y == employee)));
            _iRepositoryMock.Setup(x => x.GetAll()).Returns(employees.ToList());

            //Act
            _employeeService.PostEmployee(employee);
            employees.Add(employee);

            //Assert
            Assert.AreEqual(1, employees.Count);
            _iRepositoryMock.VerifyAll();
        }

        [Test, Category("InValid Insert")]
        public void PostEmployee_CreateNewEmployee_ExpectedInvalidOperationException()
        {
            //Arrange
            List<Employee> employees = new List<Employee>();

            var employee2 = new Employee
            {
                Id = 1,
                EmployeeId = "EMP00001",
                FirstName = "Naim",
                LastName = "Elias",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            var employee1 = new Employee
            {
                Id = 2,
                EmployeeId = "EMP00001",
                FirstName = "Naim",
                LastName = "Elias",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            employees.Add(employee2);

            _iRepositoryMock.Setup(x => x.GetAll()).Returns(employees.ToList());

            //Act
            //Assert
            Assert.Throws<InvalidOperationException>(() => _employeeService.PostEmployee(employee1));
            _iRepositoryMock.VerifyAll();
        }

        [Test, Category("GetElemnetById")]
        public void GetEmployee_GetEmployeById_ExpectedTrue()
        {
            //Arrange
            int id = 1;
            var employees = new List<Employee>();

            var employee2 = new Employee
            {
                Id = 1,
                EmployeeId = "EMP00001",
                FirstName = "Naim",
                LastName = "Elias",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            var employee1 = new Employee
            {
                Id = 2,
                EmployeeId = "EMP00002",
                FirstName = "Naim",
                LastName = "Elias",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            employees.Add(employee2);
            employees.Add(employee1);

            _iRepositoryMock.Setup(x => x.Get(id)).Returns(employees.Single(p => p.Id == id));

            //Act
            var obj = _employeeService.GetEmployee(id);

            //Assert
            Assert.AreEqual(2, employees.Count);
            Assert.AreEqual(employees[0], obj);
            _iRepositoryMock.VerifyAll();
        }

        [Test, Category("GetAllElements")]
        public void GetEmployees_GetAllElements_ExpectedTrue()
        {
            //Arrange
            var employees = new List<Employee>();
            IEnumerable<Employee> temporaryEnumList;

            var employee2 = new Employee
            {
                Id = 1,
                EmployeeId = "EMP00001",
                FirstName = "Naim",
                LastName = "Elias",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            var employee1 = new Employee
            {
                Id = 2,
                EmployeeId = "EMP00002",
                FirstName = "Rahim",
                LastName = "Kabir",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            employees.Add(employee2);
            employees.Add(employee1);

            _iRepositoryMock.Setup(x => x.GetAll()).Returns(employees.ToList());

            //Act
            temporaryEnumList = _employeeService.GetEmployees("Naim", null);

            //Assert
            Assert.AreEqual(1, temporaryEnumList.Count());
            _iRepositoryMock.VerifyAll();
        }

        [Test, Category("Delete")]
        public void DeleteEmployee_DeleteAnValidObject_ExpectedTrue()
        {
            //Arrange
            int id = 1;
            var employees = new List<Employee>();

            var employee1 = new Employee
            {
                Id = 1,
                EmployeeId = "EMP00001",
                FirstName = "Naim",
                LastName = "Elias",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            var employee2 = new Employee
            {
                Id = 2,
                EmployeeId = "EMP00002",
                FirstName = "Naim",
                LastName = "Elias",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            employees.Add(employee1);
            employees.Add(employee2);

            _iRepositoryMock.Setup(x => x.Get(id)).Returns(employees.Single(p => p.Id == id));
            _iRepositoryMock.Setup(x => x.Delete(It.IsAny<Employee>()));

            //Act
            _employeeService.DeleteEmployee(id);
            //Assert
            _iRepositoryMock.VerifyAll();
        }

        [Test, Category("Update")]
        public void PuTEmployee_UpdateAnValidObject_ExpectedTrue()
        {
            //Arrange
            int id = 1;
            var employees = new List<Employee>();

            var employee1 = new Employee
            {
                Id = 1,
                EmployeeId = "EMP00001",
                FirstName = "Naim",
                LastName = "Elias",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            var employee2 = new Employee
            {
                Id = 2,
                EmployeeId = "EMP00002",
                FirstName = "Naim",
                LastName = "Elias",
                Gender = "Male",
                Religion = "Islam",
                DateofBirth = DateTime.Now,
                EmailOfficial = "enosis@enosisbd.com",
                EmailPersonal = "enosis@gamil.com",
                PhonePrimary = "0123456789",
                PhoneSecondary = "0123456789",
                Address = "Mirpur",
                EmploymentType = "Full Type",
                Designation = "Software Engineer",
                Department = "Software Engineering",
                JoiningDate = DateTime.Now.AddYears(5),
                ResignationDate = DateTime.Now.AddYears(10)
            };

            employees.Add(employee1);
            employees.Add(employee2);

            employee1.FirstName = "Rahim";

            _iRepositoryMock.Setup(x => x.Get(id)).Returns(employees.Single(p => p.Id == id));
            _iRepositoryMock.Setup(x => x.GetAll()).Returns(employees.ToList());
            _iRepositoryMock.Setup(x => x.Update(It.Is<Employee>(y => y == employee1)));

            //Act
            _employeeService.PutEmployee(id, employee1);
            //Assert
            _iRepositoryMock.VerifyAll();
        }

    }
}